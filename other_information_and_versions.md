
# Other information
As mentioned previously, we started to classify two families of ncRNAs which are SNORD and TRNA so we did the same for the two families of HACA and U1, finally, we made a multi-class classification of these 4 families together in using our implemented deep learning models.

# Versions
Tensorflow : 2.12.0
NVIDIA-SMI : 530.30.02
Driver Version: 530.30.02
CUDA Version: 12.1
cuDNN : 8.1.0 