
# SRNA_Classification

This project, titled "SRNA Classification", aims to classify families of small RNAs. It investigates whether RNA sequence, RNA secondary structure, or both can better classify two families belonging to non-coding RNAs. The deep learning models used in this project are convolutional neural networks.

The project directory has several folders, serving distinct purposes :

## Data

The data folder comprises all data used in the project.

## Functions

The functions folder contains the libraries developed for use in the project notebooks.

## Models

The models folder encompasses the different deep learning models created for the project.

## Presentation of the Week

The presentation of the week folder comprises the weekly presentation that is presented to the internship supervisors, serving as a weekly report.

## RNAfold

The RNAfold folder contains two subfolders, each representing the data of a family. The data contained within each folder generate two new files for each sequence: the matching matrix and its secondary structure. This information is obtained through the use of rnafold.

## Results

The results folder contains all the project results.

## Python Script

The Python script folder contains all the Python scripts used in the project.

## Python Keras Script

The Python Keras Script folder contains Python scripts that were consulted to create the project's own models. These scripts serve as examples of Keras models that can be found online.

